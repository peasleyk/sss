FROM golang:latest AS builder

COPY main.go /app/
COPY checker.go /app/
COPY server.go /app/
COPY ping.go /app/
COPY status.go /app/
COPY docker.go /app/
COPY go.mod /app/
COPY go.sum /app/
COPY config.json /app/
COPY ./static/ /app/static

WORKDIR /app
RUN ls
RUN go get && \
    CGO_ENABLED=0 go build -ldflags '-extldflags "-static"' -o sss

FROM alpine
COPY --from=builder /app/ /app/
RUN apk add iputils
WORKDIR /app/
CMD ["./sss"]
