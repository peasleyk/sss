# Stupid Simple Status

No javascript, no databases. A super simple dashboard of websites, their response time, and their status codes

![out](out.png)

# Setting Up Services

```config.yml``` holds all the services to check, they will be checked sequentially.

See ```config.yml.example``` on how to set it up. We need a host name and a type of check

## Checks
```Ping``` doesn't use actually ping, but measures connection time from connection initiation to connection accepted

```Status``` checks http response codes for an endpoint or domain

```Docker``` checks containers running on the host machine for their uptime

And on the top level, we need a refresh rate in minutes for how often to check everything

Recent errors are removed after 24 hours. A duplicate error will extend the time limit.

# Building
### Go (native)
To build and run:
```shell
$ go build
$./sss -help
Usage of ./sss:
  -log string
      Where to log {stderr, file, both} (default "stderr")
  -port string
      Port to run on (default "8080")
```
or with mage
```shell
$ mage buildr; mage run
```

### Docker
If you don't have go installed but have docker build and run it:
```shell
docker-compose up
```
This will start it in live mode, with config volumed it. This way you can just 
restart the container on file change instead of rebuilding it