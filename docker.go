package main

import (
	"context"
	"fmt"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
)

type Docker struct {
	Name      string `yaml:"Name"`
	Container string `yaml:"Container"`
	Type      string
	Label     string `yaml:"Label"`

	Down   bool
	Uptime string
}

// Tries to find defined running containers and their uptimes
func (d *Docker) check() error {
	cli, err := client.NewClientWithOpts(client.WithVersion("1.37"))
	if err != nil {
		return err
	}

	containers, err := cli.ContainerList(context.Background(), types.ContainerListOptions{})
	if err != nil {
		return err
	}

	for _, container := range containers {
		for _, name := range container.Names {
			println(name)
			if name == d.Name {
				d.Uptime = container.Status
				d.Down = false
				return nil
			}
		}
	}
	d.Uptime = "0 minutes"
	d.Down = true
	return fmt.Errorf("Container %s not running", d.Name)
}
