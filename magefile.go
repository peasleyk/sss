//+build mage

// Mage build script for My blog. See options below
package main

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

// Globals used in jobs below
var BINARY_NAME = "sss"
var BUILD_DIR = "./bin"
var BINARY_PATH = filepath.Join(BUILD_DIR, BINARY_NAME)
var BUILD_PARAMS = "-ldflags=-s -w"
var Default = Dev

// Builds the binary.
func Build() error {
	gocmd := mg.GoCmd()
	return sh.RunV(gocmd, "build", "-o", BINARY_PATH)
}

// Builds a release version with more checks
func BuildR() error {
	mg.Deps(Clean, Check)
	gocmd := mg.GoCmd()
	return sh.RunV(gocmd, "build", BUILD_PARAMS, "-o", BINARY_PATH)
}

// Starts the development server
// This opens a firefox page to the website, as well as reloads the posts
// on file change
func Dev() (err error) {
	mg.Deps(Build)
	port := port()
	// gocmd := mg.GoCmd()
	if err := sh.RunV("firefox", fmt.Sprintf("localhost:%s", port)); err != nil {
		return err
	}
	if err := sh.RunV("bash", "-c", "ls | entr mage build && ./"+BINARY_PATH+" -port "+port); err != nil {
		return err
	}
	return nil
}

// Run the server in production mode
func Run() error {
	port := port()
	if err := sh.Run(BINARY_PATH, "-port", port); err != nil {
		return err
	}
	return nil
}

// Remove the temporarily generated files from Build.
func Clean() error {
	cmd := mg.GoCmd()
	err := sh.RunV(cmd, "clean")
	err = sh.Rm(BUILD_DIR)
	if err != nil {
		return err
	}
	return nil
}

// Builds and runs a docker container
func Docker() error {
	port := port()
	if err := sh.RunV("docker", "build", "--force-rm", "-t", BINARY_NAME+":latest", "."); err != nil {
		return err
	}
	if err := sh.RunV("docker", "run", "--rm", "--init", "-p", fmt.Sprintf("%s:%s", port, port), BINARY_NAME+":latest"); err != nil {
		return err
	}
	return nil

}

// Go tools to run before committing
// This sticks to built in tooling for now
func Check() {
	cmd := mg.GoCmd()
	// Neat way to kick off an anonymous function
	defer func() {
		{
			sh.RunV(cmd, "fmt")
			sh.RunV(cmd, "vet")
			sh.RunV(cmd, "mod", "tidy")
			sh.RunV(cmd, "mod", "verify")
			sh.RunV("golangci-lint", "run")
			sh.RunV(cmd, "clean")
		}
	}()
}

// Determines what port to use
func port() string {
	p := os.Getenv("PORT")
	if p == "" {
		return "12345"
	}
	return p
}
