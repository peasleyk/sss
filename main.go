package main

import (
	"flag"
	"log"
	"time"
)

type Configuration struct {
	Refresh_Rate int         `yaml:"Refresh_Rate"`
	Dockers      []Docker    `yaml:"Docker"`
	Pingers      []Pinger    `yaml:"Ping"`
	Requesters   []Requester `yaml:"Status"`
}

type Checker interface {
	check() error
}

type StatusHolder struct {
	Dockers    []Checker
	Pingers    []Checker
	Requesters []Checker
	Stamp      string
	Limit      int
	Errors     map[string]*TimedError
	Log        *log.Logger
}

type TimedError struct {
	Count     int
	Time      time.Time
	HumanTime string
}

func (t *TimedError) format() {
	t.HumanTime = t.Time.Format("1:04:05 PM")
}

func (s *StatusHolder) process(err error) {
	if err != nil {
		if _, ok := s.Errors[err.Error()]; ok {
			s.Errors[err.Error()].Count++
			s.Errors[err.Error()].Time = time.Now()
			s.Errors[err.Error()].format()

		} else {
			te := TimedError{
				Time:  time.Now(),
				Count: 1,
			}
			s.Errors[err.Error()] = &te
			s.Errors[err.Error()].format()
			s.Log.Print(err.Error())
		}
	}
}

func main() {
	port := flag.String("port", "12345", "Port to run on")
	loggingOutPut := flag.String("log", "stderr", "Where to log {stderr, file, both}")

	flag.Parse()
	location := "config.yml"
	configuration, err := loadConfig(location)
	if err != nil {
		log.Fatal(err)
	}

	// Create a slice of interfaces to loop over
	// Make a constructor
	var s StatusHolder
	s.Limit = configuration.Refresh_Rate
	for x := range configuration.Dockers {
		configuration.Dockers[x].Type = "Docker"
		s.Dockers = append(s.Dockers, Checker(&configuration.Dockers[x]))
	}
	for x := range configuration.Pingers {
		configuration.Pingers[x].Uptime = time.Now()
		configuration.Pingers[x].Type = "Ping"
		s.Pingers = append(s.Pingers, Checker(&configuration.Pingers[x]))
	}
	for x := range configuration.Requesters {
		configuration.Requesters[x].Type = "Status"
		configuration.Requesters[x].Uptime = time.Now()
		s.Requesters = append(s.Requesters, Checker(&configuration.Requesters[x]))
	}

	// Attach the server to our information struct to allow the template
	// access to the info
	logger := getLogger(*loggingOutPut, 3)
	s.Log = logger
	server := s.createServer(*port)

	go server.ListenAndServe()

	//Start this in another thread...
	s.Errors = make(map[string]*TimedError)
	for {
		for index := range s.Dockers {
			s.process(s.Dockers[index].check())
		}
		for index := range s.Pingers {
			s.process(s.Pingers[index].check())
		}
		for index := range s.Requesters {
			s.process(s.Requesters[index].check())
		}
		// Remove errors that are older than a day
		for err := range s.Errors {
			if time.Since(s.Errors[err].Time).Hours() > 24 {
				delete(s.Errors, err)
			}
		}
		s.Stamp = time.Now().Format(time.Stamp)
		time.Sleep(time.Duration(configuration.Refresh_Rate) * time.Minute)
	}
}
