package main

import (
	"net/http"
	"net/http/httptrace"
	"time"
)

type Pinger struct {
	Name           string `yaml:"Name"`
	Host           string `yaml:"Host"`
	Type           string
	Degrade_Thresh int    `yaml:"Degrade_thresh"`
	Label          string `yaml:"Label"`

	Degraded     bool
	Uptime       time.Time
	DisplayTime  string
	ResponseTime time.Duration
}

// Uses httptrace to find the total time taken for a connection establishment, similar to ping
func (p *Pinger) check() error {
	var connect time.Time
	req, _ := http.NewRequest("GET", p.Host, nil)
	trace := &httptrace.ClientTrace{
		ConnectStart: func(network, addr string) { connect = time.Now() },
		ConnectDone: func(network, addr string, err error) {
			p.ResponseTime = time.Since(connect)
		},
	}
	req = req.WithContext(httptrace.WithClientTrace(req.Context(), trace))
	res, err := http.DefaultTransport.RoundTrip(req)
	// Release out fds
	if err == nil {
		defer res.Body.Close()
	}
	if err != nil {
		p.Uptime = time.Now()
		p.DisplayTime = checkTime(p.Uptime)
		p.ResponseTime = 0
		return err
	}
	p.Degraded = false
	if int(p.ResponseTime)/1000000 > p.Degrade_Thresh {
		p.Degraded = true
	}
	p.DisplayTime = checkTime(p.Uptime)
	return nil
}
