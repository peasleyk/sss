package main

import (
	"bytes"
	"flag"
	"net/http"
	"text/template"
	"time"
)

var templates = template.Must(template.ParseFiles("static/index.tmpl",
	"static/ping.tmpl",
	"static/status.tmpl",
	"static/docker.tmpl",
	"static/errors.tmpl",
))

//Remove case, add tmpl error catch
func (s *StatusHolder) rootHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.Error(w, "404", http.StatusInternalServerError)
		return
	}

	err := renderTemplate(w, "index", s)
	if err != nil {
		panic(err)
	}
}

// Render a template with some data - utility to handle errors
func renderTemplate(w http.ResponseWriter, tmpl string, d interface{}) error {
	// ExecuteTemplate takes in a io.Writer, which bytes.buffer happens to
	// satisfy io.Writer interface. Therefore to avoid having a half rendered template
	// we can 'try' writing to that buffer. If that works, we can then
	// write that buffer to our ResponseWriter
	// https://stackoverflow.com/q/30821745/
	buf := &bytes.Buffer{}
	err := templates.ExecuteTemplate(buf, tmpl+".tmpl", d)
	if err != nil {
		return err
	}
	// Template is fine, go and write it out
	_, err = buf.WriteTo(w)
	if err != nil {
		return err
	}

	return nil
}

// We return a value to a pointer, because http.server contains a mutex
func (s *StatusHolder) createServer(port string) *http.Server {
	flag.Parse()
	serverMux := http.NewServeMux()
	serverMux.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))
	serverMux.HandleFunc("/", s.rootHandler)
	srv := &http.Server{
		Addr:         ":" + port,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  120 * time.Second,
		Handler:      serverMux,
	}
	s.Log.Printf("Starting Server on port %s", port)
	return srv
}
