package main

import (
	"fmt"
	"math"
	"net/http"
	"time"
)

type Requester struct {
	Name         string `yaml:"Name"`
	Host         string `yaml:"Host"`
	Type         string
	ExpectedCode int    `yaml:"Code"`
	Label        string `yaml:"Label"`

	Code        int
	DisplayTime string
	Uptime      time.Time
}

// Checks https response for a given url
func (r *Requester) check() error {
	resp, err := http.Get(r.Host)
	if err != nil {
		r.Uptime = time.Now()
		r.DisplayTime = checkTime(r.Uptime)
		return err
	}
	defer resp.Body.Close()
	r.Code = resp.StatusCode
	r.DisplayTime = checkTime(r.Uptime)
	return nil
}

func checkTime(thenTime time.Time) string {
	out := fmt.Sprintf("%v minutes", math.Round(time.Since(thenTime).Minutes()))
	if time.Since(thenTime).Hours() > 1 {
		out = fmt.Sprintf("%v Hours", math.Round(time.Since(thenTime).Hours()))
	}
	if time.Since(thenTime).Hours() > 24 {
		out = fmt.Sprintf("%v Days", math.Round(time.Since(thenTime).Hours()/24))
	}
	return out
}
