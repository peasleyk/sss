package main

import (
	"io"
	"io/ioutil"
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

func getLogger(mode string, flag int) *log.Logger {
	var l log.Logger
	l.SetFlags(flag)
	l.SetPrefix("[sss] ")
	var f *os.File

	if mode == "file" || mode == "both" {
		f, _ = os.OpenFile("log.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	}
	switch mode {
	case "file":
		l.SetOutput(f)
	case "both":
		multi := io.MultiWriter(os.Stdout, f)
		l.SetOutput(multi)
	case "stderr":
		l.SetOutput(os.Stderr)
	}
	return &l
}

func loadConfig(location string) (Configuration, error) {
	configuration := Configuration{}
	file, err := ioutil.ReadFile(location)
	if err != nil {
		return configuration, err
	}
	err = yaml.Unmarshal([]byte(file), &configuration)
	if err != nil {
		return configuration, err
	}
	return configuration, nil
}
